* HaptiClock
HaptiClock is a haptic clock device which communicates the time via haptics. Currently, it runs on a Raspberry Pi Pico W board, which uses the RP2040 microcontroller, and uses the following components:
1. [[https://www.adafruit.com/product/2305][Adafruit Haptic Controller]] - 2
2. Haptic motors ([[https://www.adafruit.com/product/1201][also from Adafruit]]) - 2
3. [[https://www.adafruit.com/product/1982][Adafruit 12-key Capacitive Touch Sensor]] - 1
4. Force-sensitive resistor ([[https://www.adafruit.com/product/166][also from Adafruit]]) - 1
5. Woven conductive fabric ([[https://www.adafruit.com/product/1168][also from Adafruit]])

HaptiClock is currently in development on a breadboard, and the [[file:circuit-photo.png][circuit-photo.png]] file shows the circuitry as of commit =303a21c=. We plan to use plastic housing shaped like a computer mouse, and will have a 3D-printed model within one week.

_How It Works_
HaptiClock works by using haptic vibrations to communicate the time. The user begins by touching two motors, each with one finger. The motors will then vibrate a certain number of times. Currently, the left motor communicates the hours and the right motor the minutes. Specifically, the left motor will buzz once per hour; if the hours are greater than twelve, the left motor begins with a double buzz haptic effect. Similarly, the right motor will buzz once per every five minutes, beginning with a double buzz if the minutes are greater than 30.

For example, if the time is 6:46 AM, the left motor will buzz six times and then the right motor will buzz nine times (46 minutes divided into groups of 5 minutes, rounding down, is nine). Some more examples, expressed compactly:
- 6:49 AM: left buzz 6x -> right buzz 10 (rounding 49min up to 50min)
* Install
The codebase is being developed on a Pico W with the following characteristics:
- =os.uname()=: ~(sysname='rp2', nodename='rp2', release='1.23.0', version='v1.23.0 on 2024-06-02 (GNU 13.2.0 MinSizeRel)', machine='Raspberry Pi Pico W with RP2040')~
* Roadmap
** Hardware
*** TODO Add RTC
Find one that has a compatible library. Probably Adafruit.
*** TODO Add humistor and thermistor
*** Archive :ARCHIVE:
**** DONE Test haptic motor range
:PROPERTIES:
:ARCHIVE_TIME: 2024-09-07 Sat 23:28
:END:
Observe motor response for 2-5V input.
** Software
*** Time Protocols
**** TODO Add settings for HHMM protocol
**** TODO Add support for integer protocol
**** TODO Add settings for integer protocol
**** TODO Add support for MM-To-HH mode
e.g. it's 10 minutes to 7pm.
**** TODO Add settings for MMHH mode
**** TODO Add support for 12 hour time
*** TODO Properly remove async tasks on settings update
*** TODO Subdivide settings into per-time protocol settings
*** TODO Use json instead of csv for light data
*** TODO Optimize prepareLightLevelTemplateParams()
- [ ] use json
- [ ] find max better
*** TODO HTML button that has the device exit AP mode
"Warning: with AP mode enabled, HaptiClock may consume more power or run slightly slower."
*** TODO Filesystem space left checks for data
*** TODO Figure out ntptime =ETIMEDOUT=
Traceback:
#+begin_src
Both capacitive sensors touched.
Task exception wasn't retrieved
future: <Task> coro= <generator object 'run' at 20026b20>
Traceback (most recent call last):
  File "asyncio/core.py", line 1, in run_until_complete
  File "Hapticlock_min.py", line 115, in run
  File "Hapticlock_min.py", line 83, in checkCapacitiveEvents
  File "Hapticlock_min.py", line 75, in buzzTime
  File "Hapticlock_min.py", line 74, in getHHMM
  File "ntptime.py", line 1, in time
OSError: [Errno 110] ETIMEDOUT
#+end_src
*** TODO Ntptime n tries before quit
*** TODO Allow runtime addition of async tasks
If the user enables monitoring light, then that task should be added to the event loop /without/ rebooting, as would currently be required. Take care to implement light monitoring in such a way that if the user enables it at special times, like right before the start or stop time, that monitoring proceeds as it should.

A conditional is necessary to limit measurements to between the start and stop time, and to not take measurements if monitoring is enabled right before the stop time.

If monitoring start time is 23:00, and it is enabled at 23:01, i.e. one minute after it should have started, the system should take a reading. Maybe it should take a reading whenever it's enabled, if time is between start and stop? And if a blocking event happens during a data point collection time, e.g. 23:30, then that data should still be collected.
*** TODO Light level live feed
*** TODO Temperature live feed
*** TODO Consider how to communicate 00
If HH or MM is 00, or rounds down to 00, how should that be communicated? The user should not end up thinking they've missed the minutes, or that it's not working.
*** TODO Add RTC
*** TODO Add support for concurrent haptics
*** TODO Calibration mode for capacitive touch
Is this even necessary?
*** TODO Calibration for haptic drivers
*** TODO Connecting to WiFi n tries before quit
*** TODO Double tap for configuration mode
*** TODO WiFi vs. AP control flow
*** TODO Implement backup
Settings and light data.
*** TODO Add general logging support
*** TODO Add try and catch as necessary
*** TODO Add mode to try all haptic effects
*** TODO Add mode to try individual haptic effect
*** TODO Let user change effects on time protocol
*** TODO Minify json on copy to Pico W
*** TODO Move to custom, tiny JS for charts
*** TODO Proper lsr data renaming per day
*** User Settings
**** TODO Get date
**** TODO Web server over WiFi
Configurable settings
- buzz intensities
- buzz lengths
- buzz wait times
**** TODO Low power mode for when plugged in
No light tracking, no force sensor checking, etc.
**** TODO Implement Pico W WiFi broadcast
Implement Pico W serving it's own WiFi network.
**** TODO Implement web server over broadcast
Implement serving =phew!= web server over Pico's own broadcast WiFi network.
**** TODO Add error/debug mode if loadSettings() failed
**** TODO Add read-only circuitSettings
With e.g. GPIO pin numbers.
**** TODO Add Phew logging support
**** New Settings
***** TODO Timezone
**** Archive :ARCHIVE:
***** DONE Determine how to store changes to configuration parameters
:PROPERTIES:
:ARCHIVE_TIME: 2024-09-28 Sat 16:33
:END:
Config file parsed by the software?
***** DONE Save settings to disk
:PROPERTIES:
:ARCHIVE_TIME: 2024-10-05 Sat 22:06
:END:
*** Archive :ARCHIVE:
**** DONE Add Hapticlock class to store all sensors
:PROPERTIES:
:ARCHIVE_TIME: 2024-09-07 Sat 23:28
:END:
**** DONE Add second capacitive touch
:PROPERTIES:
:ARCHIVE_TIME: 2024-09-07 Sat 23:28
:END:
**** DONE Rewrite =checkCapacitiveEvents()= to handle second capacitive touch
:PROPERTIES:
:ARCHIVE_TIME: 2024-09-07 Sat 23:28
:END:
**** DONE Add haptic breakout boards
:PROPERTIES:
:ARCHIVE_TIME: 2024-09-07 Sat 23:28
:END:
**** DONE Figure out class system for haptic and time sequence building and actuating
:PROPERTIES:
:ARCHIVE_TIME: 2024-09-07 Sat 23:29
:END:
**** DONE Use uasyncio for event loop
:PROPERTIES:
:ARCHIVE_TIME: 2024-10-05 Sat 21:09
:END:
**** DONE Move sensor monitoring to individual tasks
:PROPERTIES:
:ARCHIVE_TIME: 2024-10-09 Wed 22:23
:END:
This saves computations per event loop, particularly for sensor checks that don't need to be every loop. For example, the LSR should run every half hour if it is enabled in Settings and if the time is between start and stop. Instead of performing the Settings check every event loop, the LSR task should only be added to the loop if the Setting is true.
**** DONE Add force sensor
:PROPERTIES:
:ARCHIVE_TIME: 2024-10-09 Wed 22:23
:END:
**** DONE Put device in AP mode with FSR
:PROPERTIES:
:ARCHIVE_TIME: 2024-10-10 Thu 19:29
:END:
**** DONE Graph light levels
:PROPERTIES:
:ARCHIVE_TIME: 2024-10-10 Thu 22:03
:END:
** Housing
*** TODO Add armature inside
*** TODO Add thumb dimples
*** Archive :ARCHIVE:
**** DONE Determine external features
:PROPERTIES:
:ARCHIVE_TIME: 2024-10-05 Sat 18:58
:END:
- capacitive fabric overlay zone(s)
- holes for haptic motors
- hole(s) for force sensor
- hole for charging cable
- hole for battery
**** DONE Determine internal features
:PROPERTIES:
:ARCHIVE_TIME: 2024-10-05 Sat 18:58
:END:
- structural frame
- slots for haptic motors
- slots for force sensor
- battery holder
- PCB holder
**** DONE Design haptic motor tester
:PROPERTIES:
:ARCHIVE_TIME: 2024-10-05 Sat 18:58
:END:
Design a small housing to test a single haptic motor. This is necessary to tune the haptic feedback.
**** DONE Reduce wall between finger grooves to ridge
:PROPERTIES:
:ARCHIVE_TIME: 2024-10-09 Wed 19:35
:END:
**** DONE Add bed adhesion bar to front cable hole
:PROPERTIES:
:ARCHIVE_TIME: 2024-10-09 Wed 19:35
:END:
**** DONE Add stitching holes
:PROPERTIES:
:ARCHIVE_TIME: 2024-10-09 Wed 19:35
:END:
** HaptiClock DSL
It would be nice to have a Domain-Specific Language (DSL) for HaptiClock. This DSL would be an abstract wrapper around two buzzers and the DRV25605 haptic effects. Users would be able to define how they wanted the time to be received. A DSL wouldn't really add much functionality, given the low number of bits of information that the time contains and the small number of buzzers, although it would be a nice endcap on this project.
* Development
1. Upon changes to =Hapticlock.py=, run =run.sh=. This will minify =Hapticlock.py=, compile it to =Hapticlock_min.py=, and then copy it to =/lib/Hapticlock_min.mpy=.
2. Reset the Pico W (soft reset, reset, and reboot all work).
3. After reset, the Pico W will default to running =/main.py= by default. This will simply import =Hapticlock_min=, which will run the =Hapticlock.run()= method called at the end of =Hapticlock.py=.

Essentially, upload the new =Hapticlock.py= and ensure it's run by =main.py=.
* How the System Works
** Haptic Time Protocol
A Haptic Time Protocol (HTP) is necessary to define how time will be transmitted via haptics. This protocol is intended to be independent of hardware.
*** Design Characteristics
1. Precision: what time precision does the protocol allow? Seconds, minutes, five minutes?
2. Number of fingers: how many motors are used?
3. Haptic motor: which haptic motor(s) is being used to transmit the signal?
4. Haptic characteristics: what haptic sequences are used? A simple pulse? A decaying pulse? Double-pulses? At what intensity and duration?
*** Precision
- Seconds is unnecessary to know when waking up.
- Minutes is necessary, but maybe to nearest five or ten.
- Hours is necessary, down to nearest hour.
- Can have different protocol if before early time, e.g. 4AM
*** Number of Fingers
_One_
- Pros
  - Simpler mechanical, electrical, and software design
  - User only has to achieve correct placement of one finger
  - User only has to mentally focus on one finger
- Cons
  - User cannot differentiate data types based on different finger (must use other haptic characteristics to denote data types, e.g. communicating hours vs. minutes)

_Two_
- Pros
  - Doubles the protocol message space
  - Easy to differentiate data types with two fingers (either one, or the other)
- Cons
  - User must achieve correct placement of two fingers
  - More complex design

_Three and above_
- Pros
  - Even greater protocol message space
- Cons
  - Requires some mental effort to differentiate data types with three fingers (not simply binary, as it was with two fingers)
  - User must achieve placement of three fingers
  - More complex design
** Light Level Recording
HaptiClock has a light level measuring system designed to measure light levels throughout the night and present this information to the user. The sensor is a simple photoresistor (light-sensitive resistor, or LSR) voltage-divider. The prototype's LSR has a resistance in the hundreds of MegaOhms (measured on an Ohmeter) in darkness, and in a room with one typical overhead light has a resistance of about 10kOhm.

Thus, to measure light levels with decent resolution at night, the LSR should be in series with a resistor on the order of tens of MegaOhms. Currently, the prototype uses a 15MOhm series resistor.
** Settings
There are several types of settings: user settings, time protocol settings, and device settings. _User Settings_ are preferences for the user to choose, e.g. whether to enable the FSR for confirming the time, or whether or monitor light levels in the room. _Time Protocol Settings_ are settings specific to each time protocol. While these are technically User Settings, they can potentially grow to be extensive and thus deserve their own category. In the future, it would be nice to allow users to construct their own Time Protocol in a HaptiClock Domain-Specific Language (DSL). Thus, the Time Protocol Settings are not really a type of setting or preference, they are more a description of data transfer, albeit currently highly constrained and incomplete. _Device Settings_ are settings that affect the circuit, such as GPIO pin numbers, and are not configurable by users short of uploading a new file to the device.

Between the settings JSON dictionary, the settings referenced in the code, and the settings web interface HTML template, it's worth going over what keys and values are stored where, and how the whole system works. _User Settings_ are stored in =settings.json= and are loaded into a dictionary in memory in =HaptiClock.__init__()=. They can be changed via the web interface, which writes those changes to the disk and also changes the value in memory.

In =settings.json=, a Setting is a key-value pair: =settingKey -> settingValue=.
#+begin_src json
{"settingKey": "settingValue"}
#+end_src

Because =settings.json= is loaded into the code as is, i.e. a dict, the =settingKey= values must match those referenced in the code:
#+begin_src python
self.settings: dict = self.loadSettings()

def hapticlockCode(self):
    if self.settings["settingKey"] == value:
        pass
#+end_src

The code updates Settings with a POST request from a web interface. Requests are parsed as key-value pairs: =settingKeyName->value=. Because =self.settingKey= is set to the value, =settingKeyName= and =settingKey= can differ.
#+begin_src python
@server.route("/submit", methods=["POST"])
def settingsForm(req):
    self.settings["settingKey"] = req.form.get("settingKeyName")
#+end_src

The web interface has a standard POST form. Here, the values of the =for= and =id= attributes must match. The value of =name= must match the =settingKeyName= key in the code.
#+begin_src html
<form action="/submit" method="POST">
  <label for="settingKeyID">SettingKeyDescription</label>
  <input id="settingKeyID" name="settingKeyName">
</form>
#+end_src

At this point, we can summarize which settings parameters must match:
#+begin_src text
settingKey == self.setting["settingKey"] <- req.form.get("settingKeyName") == <input name="settingKeyName">
#+end_src

The web interface also displays the settings as a table (which is itself the form). The table has columns for Setting Name, Description, and Value. The form's =input= elements must also contain the correct =type= attribute, and the value should be set to the current setting. Finally, these values are all filled by =phew!='s templating system.
